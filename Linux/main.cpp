#include <irrlicht.h>
#include <irrDynamics.h>
#include <iostream>
#define CURL_STATICLIB
#include <curl/curl.h>
#include <string>
#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#include "micah.hpp"
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
#pragma comment(lib, "libIrrlicht.a")
#pragma comment(lib, "libcurl.la")
#pragma comment(lib, "libirrDynamicsD.so")

static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::string whirld;

int main() {
    int exitCode = -1;
    std::string theUrl = "";
    std::cout << "enter a url>";
    std::cin >> theUrl;
    if(micah::getStrPart(theUrl, 0, 7, ' ') == "cache://"){
    	std::cout<<"got cache marker\n";
    	char pathStrBuf[FILENAME_MAX];
    	GetCurrentDir(pathStrBuf, FILENAME_MAX);
    	std::string current_working_dir(pathStrBuf);
        theUrl = "file://" + current_working_dir + "/WhirldCache/" + micah::getStrPart(theUrl, 8, theUrl.length(), ' ');
    }
    CURL* curl;
    CURLcode res;
    std::string readBuffer;
    int urlError;
    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, theUrl.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        whirld = readBuffer;
    }
    std::cout << res << "\n";
    if (res != 0) { std::cout << "\n\nERROR!\nUnable to fetch whirld\n\n"; sleep(2); return 3; }

    IrrlichtDevice* device =
        createDevice(video::EDT_OPENGL, dimension2d<u32>(640, 480), 16,
            false, false, false, 0);

    if (!device)
        return 1;
    device->setWindowCaption(L"Whirld");
    IVideoDriver* driver = device->getVideoDriver();
    ISceneManager* smgr = device->getSceneManager();
    IGUIEnvironment* guienv = device->getGUIEnvironment();
    
    int d = 0;
    char s = whirld[d];
    int objects = 0;
    std::string temp = "";
    ISceneNode* world = smgr->addEmptySceneNode();
    IMeshSceneNode* theObject = smgr->addCubeSceneNode(10.0f, world);
    theObject->setVisible(false);
    vector3df objCoord = vector3df(0.0f, 0.0f, 0.0f);
    vector3df spawnPos = vector3df(0.0f, 0.0f, 0.0f);
    int objType = 0;
    while (true) {
        if (whirld.length() < 2) { exitCode = 1;  break; }//the file is empty
        s = whirld[d];
        if (s == ';') {
            d += 1;
            s = whirld[d];
            temp = "";
            objType = 0;
        }
        else if (s == '>') { exitCode = 0; break; }//all went well (probably)
        else if (s == '<') {
            d += 1;
            s = whirld[d];
        }
        else { exitCode = 2; break; }//missing or unexpected character between object data
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (temp == "Cube") {
            theObject = smgr->addCubeSceneNode(10.0f, world);
            objects += 1;
            objType = 0;
        }
        else if (temp == "Sphere") {
            objType = 0;
            theObject = smgr->addSphereSceneNode(5.0f, 16, world);
            objects += 1;
        }
        if (temp == "Spawn") {
            objType = 1;
        }
        else { 
            theObject->setMaterialFlag(EMF_LIGHTING, false);
        }

        objCoord = vector3df(0.0f, 0.0f, 0.0f);
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 1) {
            spawnPos.X = std::strtof(temp.c_str(), NULL);
        }
        else {
            objCoord.X = std::strtof(temp.c_str(), NULL) * 10;
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 1) {
            spawnPos.Y = std::strtof(temp.c_str(), NULL);
        }
        else {
            objCoord.Y = std::strtof(temp.c_str(), NULL) * 10;
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 1) {
            spawnPos.Z = std::strtof(temp.c_str(), NULL);
        }
        else {
            objCoord.Z = std::strtof(temp.c_str(), NULL) * 10;
        }
        //std::cout << objCoord.X << ", " << objCoord.Y << ", " << objCoord.Z << "\n";
        theObject->setPosition(objCoord);
        //std::cout << theObject->getPosition().X << ", " << theObject->getPosition().Y << ", " << theObject->getPosition().Z << "\n";

        objCoord = vector3df(0.0f, 0.0f, 0.0f);
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.X = std::strtof(temp.c_str(), NULL);
            theObject->setRotation(objCoord);
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.Y = std::strtof(temp.c_str(), NULL);
            theObject->setRotation(objCoord);
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.Z = std::strtof(temp.c_str(), NULL);
            theObject->setRotation(objCoord);
        }

        objCoord = vector3df(0.0f, 0.0f, 0.0f);
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.X = std::strtof(temp.c_str(), NULL);
            theObject->setScale(objCoord);
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ',') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.Y = std::strtof(temp.c_str(), NULL);
            theObject->setScale(objCoord);
        }
        temp = "";
        d += 1;
        s = whirld[d];
        while (s != ';' && s != '>') {
            temp += s;
            d += 1;
            s = whirld[d];
        }
        if (objType == 0) {
            objCoord.Z = std::strtof(temp.c_str(), NULL);
            theObject->setScale(objCoord);
        }
    }
    if (exitCode == 0 && objects >= 1) { std::cout << "Successfully loaded whirld loaded with " << objects << " objects\n"; }
    else if (exitCode == 1) { std::cout << "\n\nERROR!\nInvalid whirld file: \nFile too small to be a whirld file\n\n"; sleep(2); device->drop(); return 1; }
    else if (exitCode == 2) { std::cout << "\n\nERROR!\nInvalid whirld file: \nUnexpected characters\n\n"; device->drop(); sleep(2); return 2; }
    else { std::cout << "\n\nERROR!\nUnknown error\n\n"; sleep(2); device->drop(); return -1; }
    if (objects <= 0) {std::cout << "\n\nERROR!\nInvalid whirld file: \nNo objects were created\n\n"; device->drop(); sleep(2); return 3;}

    SKeyMap keyMap[8];
    keyMap[0].Action = EKA_MOVE_FORWARD;
    keyMap[0].KeyCode = KEY_UP;
    keyMap[1].Action = EKA_MOVE_FORWARD;
    keyMap[1].KeyCode = KEY_KEY_W;

    keyMap[2].Action = EKA_MOVE_BACKWARD;
    keyMap[2].KeyCode = KEY_DOWN;
    keyMap[3].Action = EKA_MOVE_BACKWARD;
    keyMap[3].KeyCode = KEY_KEY_S;

    keyMap[4].Action = EKA_STRAFE_LEFT;
    keyMap[4].KeyCode = KEY_LEFT;
    keyMap[5].Action = EKA_STRAFE_LEFT;
    keyMap[5].KeyCode = KEY_KEY_A;

    keyMap[6].Action = EKA_STRAFE_RIGHT;
    keyMap[6].KeyCode = KEY_RIGHT;
    keyMap[7].Action = EKA_STRAFE_RIGHT;
    keyMap[7].KeyCode = KEY_KEY_D;

    //scene::ICameraSceneNode* camera = smgr->addCameraSceneNodeFPS(0, 100.0f, .3f, 0, keyMap, 0, false, 3.f);
    scene::ICameraSceneNode* camera = smgr->addCameraSceneNodeFPS(0,100.0f,0.1f,-1, keyMap, 8, false, 3.f);
    camera->setPosition(spawnPos);
    device->getCursorControl()->setVisible(false);
    while (device->run())
    {
        driver->beginScene(true, true, SColor(255, 100, 101, 140));

        smgr->drawAll();
        guienv->drawAll();

        driver->endScene();
    }
    device->drop();

    return 0;
}
