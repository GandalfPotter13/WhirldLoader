#include <iostream>
#include <string>
namespace micah{
        std::string getStrPart(std::string inputStr, int startPos, int endPos, char endChar){
        int d=startPos;
        int s=inputStr[d];
        std::string strPart;
        if(endPos == -1){
            while(s != endChar){
                s = inputStr[d];
                strPart += s;
                d++;
            }
        }
        else{
            while(d != endPos){
                    s = inputStr[d];
                    d++;
                    strPart += s;
            }
            s = inputStr[d];
            strPart += s;
        }
        return strPart;
    }
}